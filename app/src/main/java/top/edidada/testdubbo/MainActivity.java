package top.edidada.testdubbo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import cn.finalteam.okhttpfinal.BaseHttpRequestCallback;
import cn.finalteam.okhttpfinal.HttpRequest;
import okhttp3.Headers;
import okhttp3.Response;

public class MainActivity extends Activity {

    EditText first;
    EditText second;
    EditText thrid;
    EditText fourth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        first = findViewById(R.id.first);
        second = findViewById(R.id.second);
        thrid = findViewById(R.id.thrid);
        fourth = findViewById(R.id.fourth);
        findViewById(R.id.start_http).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String ip = getRealIPAddress();
                HttpRequest.get("http://"+ip+":8080/t/index.jsp",null,new BaseHttpRequestCallback<String>(){
                    @Override
                    protected void onSuccess(String s) {
                        super.onSuccess(s);
                        Toast.makeText(MainActivity.this,s,Toast.LENGTH_LONG).show();
                    }
                    public void onStart() {
                    }

                    public void onResponse(Response httpResponse, String response, Headers headers) {
                        Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                    }

                    public void onResponse(String response, Headers headers) {
                        Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                    }

                    public void onFinish() {
                    }

                    protected void onSuccess(Headers headers, String t) {
                        Toast.makeText(MainActivity.this,t,Toast.LENGTH_LONG).show();
                    }


                    /**
                     * 上传文件进度
                     * @param progress
                     * @param networkSpeed 网速
                     * @param done
                     */
                    public void onProgress(int progress, long networkSpeed, boolean done){
                    }

                    public void onFailure(int errorCode, String msg) {
                    }

                    public Headers getHeaders() {
                        return headers;
                    }

                    protected void setResponseHeaders(Headers headers) {
                        this.headers = headers;
                    }
                });
            }
        });
    }

    private String getRealIPAddress() throws IllegalArgumentException{
        String s_first = first.getText().toString();
        String s_second = second.getText().toString();
        String s_thrid = thrid.getText().toString();
        String s_fourth = fourth.getText().toString();

        try{
            Integer.parseInt(s_first);
            Integer.parseInt(s_second);
            Integer.parseInt(s_thrid);
            Integer.parseInt(s_fourth);
        }catch(Exception e){
            throw e;
        }
        return new StringBuilder(s_first).append(".").append(s_second).append(".").append(s_thrid).append(".").append(s_fourth).toString();
    }
}
